// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/'
import mavonEditor from 'mavon-editor'
import infiniteScroll from 'vue-infinite-scroll'

import 'mavon-editor/dist/css/index.css'
import Highlight from './config/highlight'

import { 
  Button, Select ,Input, Col, Row, Form, FormItem, Option,
  MessageBox,Message
} from 'element-ui';

Vue.use(Button);
Vue.use(Select);
Vue.use(Input);
Vue.use(Col);
Vue.use(Row);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Option);

Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$message = Message;

Vue.use(infiniteScroll) 
// Vue.use(moment); 
Vue.use(Highlight)
Vue.use(mavonEditor)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
