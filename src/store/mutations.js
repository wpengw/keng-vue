import {
    RECORD_USERINFO,
    GET_USERINFO,
    OUT_LOGIN
} from './mutation-types'

import {setStore, getStore} from '../config/mUtils'

export default {
    //记录用户信息
    [RECORD_USERINFO](state, info) {
        state.userInfo = info;
        state.login = true,
        // console.log(state.userInfo)
        setStore('user_id', info._id)
        // setStore('user_name', info.name)
        // setStore('user_email', info.email)
    },
    //获取用户信息存入vuex
    [GET_USERINFO](state, info) {
        // console.log(info, state.login)
        if(state.userInfo && (state.userInfo.loginName !== info.result.loginName)) {
            return
        }
        if(!state.login) {
            return
        }
        if(info.code) {
            state.userInfo = {...info.result}
            // console.log(state)
        } else {
            state.userInfo = null
        }
    },
    //退出登录
	[OUT_LOGIN](state) {
		state.userInfo = null;
		state.login = false;
	},
}