import {
    getUser,
} from '../service/api'

import {
    GET_USERINFO,
} from './mutation-types'
import { getStore } from '@/config/mUtils'

export default {
    async getUserInfo({
        commit,
        state
    }) {
        let res = await getUser({user_id: getStore('user_id')});
        
        commit(GET_USERINFO, res)
    }
}